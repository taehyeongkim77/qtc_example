#include "ros/ros.h"
//#include "std_msgs/String.h"

#include "hello_qtc/Msg_qtc_tutorial.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "qtc_tutorial_publisher");
  ros::NodeHandle nh;

  //ros::Publisher chatter_pub = nh.advertise<std_msgs::String>("chatter", 1000);
  ros::Publisher chatter_pub = nh.advertise<hello_qtc::Msg_qtc_tutorial>("chatter", 1000);

  ros::Rate loop_rate(10);

  int count = 0;
  while (ros::ok())
  {
    //std_msgs::String msg;
    //msg.data = "hello world";

    hello_qtc::Msg_qtc_tutorial msg;
    msg.stamp = ros::Time::now();
    msg.data = count++;

    ROS_INFO("send msg = %d", msg.stamp.sec);
    ROS_INFO("send msg = %d", msg.stamp.nsec);
    ROS_INFO("send msg = %d", msg.data);

    chatter_pub.publish(msg);

    ros::spinOnce();

    loop_rate.sleep();
  }

  return 0;
}
